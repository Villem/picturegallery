﻿using PictureGallery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PictureGallery.ViewModels
{
    public class AlbumsPage
    {
        public List<Album> AllAlbums { get; set; }
        public List<Picture> AlbumCoverPictures { get; set; }
    }
}
