﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace PictureGallery.Models
{
    public class Album
    {
        public int Id { get; set; }
        public string Title { get; set; }
        [Display(Name = "Added by ")]
        public string UserId { get; set; }
        [Display(Name = "Added by")]
        public ApplicationUser User { get; set; }
        [Display(Name = "Date created")]
        public DateTime DateCreated { get; private set; }
        public List<Picture> Pictures { get; set; }
        [Display(Name = "Favorite")]
        public bool IsFavorite { get; set; }
        public List<Favorite> Favorites { get; set; }
        public Album()
        {
            DateCreated = DateTime.Now;
        }
    }
}
