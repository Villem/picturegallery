﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PictureGallery.Models
{
    public class Like
    {
        public int Id { get; set; }
        public int PictureId { get; set; }
        public Picture Picture { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }      
        public bool IsLiked { get; set; }
    }
}
